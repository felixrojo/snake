import pygame as pg
from random import randrange

WINDOWS = 1000
TILE_SIZE = 25
RANGE = (TILE_SIZE // 2, WINDOWS - TILE_SIZE // 2, TILE_SIZE)
VERDE_OSCURO = (11,103,48)
tick = 0
tick_puntos = 0
puntos = 0
puntos_x = 0
puntos_y = 0
pg.init()
screen = pg.display.set_mode([WINDOWS]*2)
# imagenes
graficos = pg.image.load("imagenes/snake-graphics.png").convert_alpha()
fondo = pg.image.load("imagenes/fondo.jpg").convert()
fondo = pg.transform.scale(fondo, (1000, 1000))
x, y, w, h = 0, 190, 66, 66
rect = pg.Rect(x, y, w, h)
manzana = graficos.subsurface(rect)
manzana = pg.transform.scale(manzana, (TILE_SIZE+5, TILE_SIZE+5))
x, y, w, h = 256, 0, 56, 56
rect = pg.Rect(x, y, w, h)
cabeza_derecha = graficos.subsurface(rect)
cabeza_derecha = pg.transform.scale(cabeza_derecha, (TILE_SIZE, TILE_SIZE))
x, y, w, h = 192, 0, 56, 56
rect = pg.Rect(x, y, w, h)
cabeza_arriba = graficos.subsurface(rect)
cabeza_arriba = pg.transform.scale(cabeza_arriba, (TILE_SIZE, TILE_SIZE))
x, y, w, h = 192, 65, 56, 56
rect = pg.Rect(x, y, w, h)
cabeza_izquierda = graficos.subsurface(rect)
cabeza_izquierda = pg.transform.scale(cabeza_izquierda, (TILE_SIZE, TILE_SIZE))
x, y, w, h = 256, 65, 56, 56
rect = pg.Rect(x, y, w, h)
cabeza_abajo = graficos.subsurface(rect)
cabeza_abajo = pg.transform.scale(cabeza_abajo, (TILE_SIZE, TILE_SIZE))


# sonidos
# Inicializar el módulo de sonido
pg.mixer.init()

# Cargar el archivo de sonido
comer = pg.mixer.Sound('sonidos/comer.mp3')
terminar = pg.mixer.Sound('sonidos/terminar.mp3')

get_random_position = lambda: [randrange(*RANGE), randrange(*RANGE)]
snake = pg.rect.Rect([0, 0, TILE_SIZE -2 , TILE_SIZE -2])
snake.center = get_random_position()
length = 1
segments =[snake.copy()]
snake_dir = (0,0)
time, time_step = 0, 200
food = snake.copy()
food.center = get_random_position() 
fuente = pg.font.SysFont("Arial", 20)
texto = "Puntos ="
puntos_texto = fuente.render(texto,True, (0,0,0))
clock = pg.time.Clock()
dirs = {pg.K_w:1, pg.K_s:1, pg.K_a:1, pg.K_d:1}


while True:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            exit()
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_w and dirs[pg.K_w]:
                snake_dir = (0, -TILE_SIZE)
                dirs = {pg.K_w:1, pg.K_s:0, pg.K_a:1, pg.K_d:1}
            if event.key == pg.K_s and dirs[pg.K_s]:
                snake_dir = (0, TILE_SIZE)
                dirs = {pg.K_w:0, pg.K_s:1, pg.K_a:1, pg.K_d:1}
            if event.key == pg.K_a and dirs[pg.K_a]:
                snake_dir = (-TILE_SIZE, 0)
                dirs = {pg.K_w:1, pg.K_s:1, pg.K_a:1, pg.K_d:0}
            if event.key == pg.K_d and dirs[pg.K_d]:
                snake_dir = (TILE_SIZE, 0)
                dirs = {pg.K_w:1, pg.K_s:1, pg.K_a:0, pg.K_d:1}
                
    # update pantalla
    # screen.fill('black')
    screen.blit(fondo,(0,0))
    texto = f"Puntos = {puntos}"
    puntos_texto = fuente.render(texto,True, (0,0,0))
    screen.blit(puntos_texto,(10,10))
    
    # choque bordes y con la cola
    self_eating = pg.Rect.collidelist(snake, segments[:-1]) != -1
    if snake.left < 0 or snake.right > WINDOWS or snake.top < 0 or snake.bottom>WINDOWS or self_eating:
        snake.center, food.center = get_random_position(), get_random_position()
        length, snake_dir = 1, (0,0)
        segments = [snake.copy()]
        terminar.play()
        puntos = 0
        time_step = 200
    # comer
    if snake.center == food.center:
        puntos_x = food.x
        puntos_y = food.y
        comer.play()
        food.center = get_random_position() 
        length +=1
        puntos += 10
        tick_puntos =tick + 10
        time_step -= 2
        
        
    #  mostrar puntos al comer
    if tick_puntos > tick:
        puntos_y += -1
        puntos_texto2 = fuente.render(str("10"),True, (0,0,0))
        screen.blit(puntos_texto2,(puntos_x,puntos_y))
    # pg.draw.rect(screen, 'red', food)
    screen.blit(manzana,food)
    # pintando serpiente
    [pg.draw.rect(screen, VERDE_OSCURO, segment) for segment in segments[:-1]]
    # cola es el segmento 0
    if snake_dir == (0,0): 
        screen.blit(cabeza_derecha,segments[len(segments)-1])
    elif snake_dir[0] > 0: 
        screen.blit(cabeza_derecha,segments[len(segments)-1])
    elif snake_dir[1] < 0: 
        screen.blit(cabeza_arriba,segments[len(segments)-1])
    elif snake_dir[1] > 0: 
        screen.blit(cabeza_abajo,segments[len(segments)-1])
    elif snake_dir[0] < 0: 
        screen.blit(cabeza_izquierda,segments[len(segments)-1])
    # moviendo serpiente
    time_now = pg.time.get_ticks()
    if time_now - time > time_step:
        time = time_now
        snake.move_ip(snake_dir)
        segments.append(snake.copy())
        segments = segments[-length:]
        tick +=1
    pg.display.flip()
    clock.tick(60)